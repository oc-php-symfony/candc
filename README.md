# WORDPRESS OPENCLASSROOMS // PROJECT 2

## How to use

#### Prerequisite:
Use this repository for dockerization :  
https://gitlab.com/DamienAdam/dockercandc.git

#### Launch:

1. clone or download repository in the docker folder
2. run `docker-compose up`
3. restore database from ./dumps

If needed, backup database with `sh ./scripts/database-dump.sh` 

#### URLs

- project is at `localhost/`
- back-office is at `localhost/wp-admin/`
- phpmyadmin is at `localhost:8080/` id: root, pw: root
- wordpress account for dumped database are at `./misc/users.md`
 